<?php
/**
 * Classe gérant l'import des actions
 * @author: <info@loicg.net> Loïc Gerbaud
 *
 */

namespace Fip;

class ActionImporter {

	protected $xml_url;
	protected $actions;
    protected $user_id;

    /**
     * ActionImporter constructor.
     * @param string $xml_url Chemin du XML à importer
     * @param int $user_id Identifiant de l'utilisateur qui insère les actions
     */
	public function __construct($xml_url, $user_id=1){
		$this->xml_url = $xml_url;
        $this->user_id = $user_id;
	}

	public function loadActions()
	{
		$this->actions = simplexml_load_file( $this->xml_url );
	}

	public function getActions()
	{
		return $this->actions;
	}

	public function import()
	{
	    $this->loadActions();
		foreach($this->actions as $action)
		{
            $this->insertPost($action);
		}
	}

    /**
     * Converti les actions XML en array
     *
     * @param \SimpleXMLElement $action
     *
     * @return array
     */
	public function getPostParameters($action){
		$params = [];

        // Auteur de l'action
        $params['post_author'] = $this->user_id;

        // Titre et contenu
		$params['post_title'] = trim( (string) $action->title );
		$params['post_content'] = $this->convert_utf8_entities( trim( (string) $action->description ));

        // Statut (publié, brouillon...)
		$params['post_status'] = ((int) $action->{'mise-en-ligne'}) == 1?'publish':'draft';

        // Date de publication
        $params['post_date'] = trim((string) $action->dates->created);
        $params['post_modified'] = trim((string) $action->dates->updated);

        // Les champs personnalisés
		$params['meta_input'] = $this->getMetaInput($action);

        // Les taxonomies
        $params['tax_input'] = $this->getTaxInput($action);

		return $params;
	}

    /**
     * Prépare les "meta" sous forme d'un array
     * @param $action
     * @return array
     */
	public function getMetaInput($action)
	{
		$meta = [];

		$meta['fip-id'] = (int) $action->{'id'};
		$meta['action-phare'] = (int) $action->{'action-phare'};

		$meta['lieu-et-horaire'] = trim( (string) $action->{'lieu-et-horaire'} );
		$meta['inscription-debut'] = trim( (string) $action->dates->{'debut'} );
		$meta['inscription-echeance'] = trim( (string) $action->dates->{'echeance-inscription'} );
		$meta['date-action'] = trim( (string) $action->dates->{'evenement'} );

		$meta['lien-document'] = trim( (string) $action->{'lien-document'} );
		$meta['lien-inscription'] = trim( (string) $action->{'lien-inscription'} );

        $meta['organisme'] = trim( (string) $action->{'organisme'}->name );

		return $meta;
	}

    /**
     * Prépare les taxonomies sous forme d'un array
     * @param $action
     * @return array
     */
	public function getTaxInput($action)
    {

        $tax = [
            'thematique' => $this->getTaxonomiesIds( $this->getTaxonomySlugsList($action->fip->thematiques->action), 'thematique'),
            'service' => $this->getTaxonomiesIds( $this->getTaxonomySlugsList($action->fip->actions->action), 'service'),
        ];

        return $tax;
    }

    public function getTaxonomySlugsList($terms)
    {
        $slugs = [];
        foreach($terms as $term){
            if((int) $term->id > 0)
                $slugs[] = $this->slugifyTaxonomyName( (string) $term->libelle );
        }

        return implode(',', $slugs);
    }

    /**
     * Récupère les slugs de taxonomies enfants
     * @param $name
     * @return string
     */
    public function slugifyTaxonomyName($name)
    {
        $name = preg_replace('/^[^\/]+\//', '', $name);
        return sanitize_title($name);
    }

    /**
     * Récupère les ids de taxonomies d'après leurs slugs
     * @param string $slugs liste de slugs, séparés par des virgules
     * @param string $taxonomy la taxonomie des slugs
     * @return array
     */
    public function getTaxonomiesIds($slugs, $taxonomy)
    {
        $ids = [];
        $slugs = explode(',', $slugs);

        foreach($slugs as $slug)
        {
            $term = get_term_by('slug', $slug, $taxonomy);
            $ids[] = $term->term_id;

            // On prend également le parent, pour les respects de couleurs, etc...
            if($term->parent){
                $ids[] = $term->parent;
            }
        }

        return $ids;
    }


	public function insertPost($action)
    {
        $params = $this->getPostParameters($action);
        $params['ID'] =  $this->findPostIdByFipId( $params['meta_input']['fip-id']);

        $post_id = wp_insert_post($params);

        // Comme on n'est pas sûr que l'utilisateur ait les droits, on force l'insertion des taxonomies
        wp_set_object_terms($post_id, $params['tax_input']['thematique'], 'thematique');
        wp_set_object_terms($post_id, $params['tax_input']['service'], 'service');


        return $post_id;
    }

    /**
     * Recherche un post ayant le bon meta "fip-id"
     *
     * @param int $fip_id
     * @return int
     */
	public function findPostIdByFipId($fip_id){
	    $posts = query_posts([
	        'post_status' => 'any',
	        'meta_key' => 'fip-id',
            'meta_value' => $fip_id
        ]);

        if(count($posts))
            return reset($posts)->ID;
	    return 0;
	}

    /**
     * Nettoyage des entités UTF-8 (&#....) vers les vrais caractères
     *
     * @param $input string
     * @return string
     */
      private function convert_utf8_entities ($input){
        return preg_replace_callback("/(&#[0-9]+;)/", function($m) { return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES"); }, $input);
      }

}
