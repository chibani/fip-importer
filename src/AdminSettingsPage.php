<?php
/**
 * Created by PhpStorm.
 * User: loicg
 * Date: 20/09/2016
 * Time: 16:35
 */

namespace Fip;


class AdminSettingsPage {
	/**
	 * Holds the values to be used in the fields callbacks
	 */
	private $options;

	/**
	 * Start up
	 */
	public function __construct()
	{
		add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'page_init' ) );
	}

	/**
	 * Add options page
	 */
	public function add_plugin_page()
	{
		// This page will be under "Settings"
		add_options_page(
			'Settings Admin',
			'FIP Import',
			'manage_options',
			'my-setting-admin',
			array( $this, 'create_admin_page' )
		);
	}

	/**
	 * Options page callback
	 */
	public function create_admin_page()
	{
		// Set class property
		$this->options = get_option( 'fip_importer' );
		?>
		<div class="wrap">
			<h1>FIP Importer</h1>
			<form method="post" action="options.php">
				<?php
				// This prints out all hidden setting fields
				settings_fields( 'my_option_group' );
				do_settings_sections( 'my-setting-admin' );
				submit_button();
				?>
			</form>
		</div>
		<?php
	}

	/**
	 * Register and add settings
	 */
	public function page_init()
	{
		register_setting(
			'my_option_group', // Option group
			'fip_importer', // Option name
			array( $this, 'sanitize' ) // Sanitize
		);

		add_settings_section(
			'setting_section_id', // ID
			'Réglages', // Title
			array( $this, 'print_section_info' ), // Callback
			'my-setting-admin' // Page
		);

		add_settings_field(
			'xml_url', // ID
			'URL du flux XML des actions', // Title
			array( $this, 'xml_url_callback' ), // Callback
			'my-setting-admin', // Page
			'setting_section_id' // Section
		);
	}

	/**
	 * Sanitize each setting field as needed
	 *
	 * @param array $input Contains all settings fields as array keys
	 */
	public function sanitize( $input )
	{
		$new_input = array();

		if( isset( $input['xml_url'] ) )
			$new_input['xml_url'] = sanitize_text_field( $input['xml_url'] );

		return $new_input;
	}

	/**
	 * Print the Section text
	 */
	public function print_section_info()
	{
		print ' Attention, les réglages suivants peuvent casser le fonctionnement du plugin (et par conséquent le fonctionnement du site)';
	}

	/**
	 * Get the settings option array and print one of its values
	 */
	public function xml_url_callback()
	{
		printf(
			'<input type="text" id="xml_url" name="fip_importer[xml_url]" value="%s" />',
			isset( $this->options['xml_url'] ) ? esc_attr( $this->options['xml_url']) : ''
		);
	}

}