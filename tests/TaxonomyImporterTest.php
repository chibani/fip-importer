<?php
/**
 * Created by PhpStorm.
 * User: loicg
 * Date: 20/09/2016
 * Time: 16:59
 */

namespace Fip;
use PHPUnit\Framework\TestCase;

require_once(__DIR__.'/../../../../wp-load.php');

class TaxonomyImporterTest extends TestCase
{

    /**
     * @expectedException \Exception
     */
    public function testCannotLoadTermsFromInexistingFile()
    {
        $importer = new TaxonomyImporter('toto');
        $importer->loadTerms();
    }

    public function testLoadTerms()
    {
        $importer = new TaxonomyImporter('thematique');
        $importer->loadTerms();

        $this->assertInternalType( 'array', $importer->getTerms() );
    }

    public function testImportTerms()
    {
        // Purge des taxinomies
        $importer = new TaxonomyImporter('thematique');
        $importer->flushTerms();
        $this->assertCount(0, $importer->getDbTerms());

        // Import réel
        $importer->import();

        // Chargement des terms pour vérifier le nombre présent
        $created_terms = $importer->getDbTerms();
        $this->assertCount(41, $created_terms);
    }
}