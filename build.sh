#!/usr/bin/env bash
mkdir dist
echo "Cleaning vendors"
rm -rf ./vendor/
echo "Composer install (--no-dev)"
composer install --no-dev -o -q
echo "Git archive"
git archive --format zip master > dist/fip-importer.zip
echo "Add the vendor repository"
zip dist/fip-importer.zip -r vendor -q
echo "job's done"
