<?php
/**
 * Created by PhpStorm.
 * User: loicg
 * Date: 28/09/2016
 * Time: 11:20
 */

namespace Fip;


class TaxonomyActivator
{
    public static function fi_declare_taxonomies()
    {
        self::declare_thematique();
        self::declare_service();
    }


    // Déclaration de la taxonomy "thématique"
    protected static function declare_thematique() {

        $labels = array(
            'name'                       => _x( 'Thématiques', 'Taxonomy General Name', 'fip' ),
            'singular_name'              => _x( 'Thématique', 'Taxonomy Singular Name', 'fip' ),
            'menu_name'                  => __( 'Thématiques', 'fip' ),
            'all_items'                  => __( 'Toutes les thématiques', 'fip' ),
            'parent_item'                => __( 'Thématique parente', 'fip' ),
            'parent_item_colon'          => __( 'Parente :', 'fip' ),
            'new_item_name'              => __( 'Nouvelle thématique', 'fip' ),
            'add_new_item'               => __( 'Ajouter une thématique', 'fip' ),
            'edit_item'                  => __( 'Modifier la thématique', 'fip' ),
            'update_item'                => __( 'Mettre à jour', 'fip' ),
            'view_item'                  => __( 'Voir la thématique', 'fip' ),
            'separate_items_with_commas' => __( 'Separate items with commas', 'fip' ),
            'add_or_remove_items'        => __( 'Ajouter ou supprimer', 'fip' ),
            'choose_from_most_used'      => __( 'Choisir parmi les meilleures thématiques', 'fip' ),
            'popular_items'              => __( 'Popular Items', 'fip' ),
            'search_items'               => __( 'Search Items', 'fip' ),
            'not_found'                  => __( 'Not Found', 'fip' ),
            'no_terms'                   => __( 'No items', 'fip' ),
            'items_list'                 => __( 'Items list', 'fip' ),
            'items_list_navigation'      => __( 'Items list navigation', 'fip' ),
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => false,
        );
        register_taxonomy( 'thematique', array( 'post' ), $args );

    }

    // Déclaration de la taxonomy Service
    protected static function declare_service() {

        $labels = array(
            'name'                       => _x( 'Service', 'Taxonomy General Name', 'fip' ),
            'singular_name'              => _x( 'Service', 'Taxonomy Singular Name', 'fip' ),
            'menu_name'                  => __( 'Services', 'fip' ),
            'all_items'                  => __( 'Tous les services', 'fip' ),
            'parent_item'                => __( 'Service parent', 'fip' ),
            'parent_item_colon'          => __( 'Parente :', 'fip' ),
            'new_item_name'              => __( 'Nouveau service', 'fip' ),
            'add_new_item'               => __( 'Ajouter un service', 'fip' ),
            'edit_item'                  => __( 'Modifier le service', 'fip' ),
            'update_item'                => __( 'Mettre à jour', 'fip' ),
            'view_item'                  => __( 'Voir le service', 'fip' ),
            'separate_items_with_commas' => __( 'Separate items with commas', 'fip' ),
            'add_or_remove_items'        => __( 'Ajouter ou supprimer', 'fip' ),
            'choose_from_most_used'      => __( 'Choisir parmi les meilleurs services', 'fip' ),
            'popular_items'              => __( 'Popular Items', 'fip' ),
            'search_items'               => __( 'Search Items', 'fip' ),
            'not_found'                  => __( 'Not Found', 'fip' ),
            'no_terms'                   => __( 'No items', 'fip' ),
            'items_list'                 => __( 'Items list', 'fip' ),
            'items_list_navigation'      => __( 'Items list navigation', 'fip' ),
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => false,
        );
        register_taxonomy( 'service', array( 'post' ), $args );

    }
}