#FIP importer

Auteur : Loïc Gerbaud (info@loicg.net) pour Devisocom (bonjour@devisocom.com)

Version de PHP minimale : 5.3  

Script d\'import pour les articles de FIP  
Importe également les thématiques et services.

##Description

Récupère les \"actions\" présentes dans le flux XML de FIP pour les injecter en tant qu\'articles

