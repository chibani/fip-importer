<?php
/**
 * Plugin Name:       FIP Importer
 * Plugin URI:        http://
 * Description:       Plugin d'import d'articles depuis le XML de FIP
 * Version:           1.0.1
 * Author:            Loïc Gerbaud
 * Author URI:        http://loicg.net/
 * License:           proprietary
 * Text Domain:       fip-importer
 * Domain Path:       /lang
 */
require('vendor/autoload.php');

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Page de config du plugin
add_action('init', function(){
    if( is_admin() ){
        // Page de réglages du plugin
        $my_settings_page = new Fip\AdminSettingsPage();
    }

    // Déclaration des taxonomies
    Fip\TaxonomyActivator::fi_declare_taxonomies();

    add_action('fi_import_taxonomies_event', 'fi_import_taxonomies');
    add_action('fi_import_actions_event', 'fi_import_actions');


    fi_schedule_actions_import();
});


// Imports initiaux à l'activation du plugin
register_activation_hook(__FILE__, function(){

    // Création tâche cron unique pour importer les taxonomies
    wp_schedule_single_event(time(), 'fi_import_taxonomies_event' );

    fi_schedule_actions_import();
});

// Action à la désactivation du plugin
register_deactivation_hook(__FILE__, function(){
    //Désactivation des crons d'import
    wp_clear_scheduled_hook('fi_import_actions_event');
    wp_clear_scheduled_hook('fi_import_taxonomies_event');

    // Suppression de toutes les taxinomies

    // Suppression des thématiques
    //$thematiques_importer = new Fip\TaxonomyImporter('thematique');
    //$thematiques_importer->flushTerms();

    // Suppression des services
    //$services_importer = new Fip\TaxonomyImporter('service');
    //$services_importer->flushTerms();

});

/**
 * Fonction d'import des taxonomies
 */
function fi_import_taxonomies(){
    // Import taxonomies
    $thematiques_importer = new Fip\TaxonomyImporter('thematique');
    $thematiques_importer->flushTerms();
    $thematiques_importer->import();

    // Services
    $services_importer = new Fip\TaxonomyImporter('service');
    $services_importer->flushTerms();
    $services_importer->import();

    // Import actions
    $actions_importer = new Fip\ActionImporter(__DIR__.'/../assets/export_fip_formatted.xml');
    $actions_importer->import();
}


function fi_import_actions(){
    $options = get_option('fip_importer');
    if(!isset($options['xml_url']))
        return false; // On skip si l'url du XML n'est pas enregistrée

    $importer = new Fip\ActionImporter($options['xml_url']);
    $importer->import();
    return true;
}

function fi_schedule_actions_import()
{
    // Reprogrammation du cron
    if(! wp_next_scheduled('fi_import_actions_event')){
        wp_schedule_event(time(), 'daily', 'fi_import_actions_event');
    }
}

