<?php
/**
 * Created by PhpStorm.
 * User: loicg
 * Date: 20/09/2016
 * Time: 16:59
 */

namespace Fip;
use PHPUnit\Framework\TestCase as TestCase;

require_once(__DIR__.'/../../../../wp-load.php');


class ActionImporterTest extends TestCase  {

	/**
	 * @var ActionImporter
	 */
	protected $importer;

	/**
	 * @before
	 */
	public function setUpImporter()
	{
		$this->importer = new ActionImporter(__DIR__.'/../assets/export_fip_formatted.xml');
		$this->importer->loadActions();
	}

	public function testSlugifyTaxonomyName()
    {
        $this->assertEquals('sous-thematique', $this->importer->slugifyTaxonomyName('Thématique / Sous-Thématique'));
    }

    public function testGetTaxonomySlugsList()
    {
        $terms = json_decode('
        [
            {
                "id" : "1",
                "libelle" : "Thématique / Sous-Thématique"
            },
            {
                "id" : "12",
                "libelle" : "Parcours professionnel / Changement de carrière"
            }
        ]
        ');

        $this->assertEquals('sous-thematique,changement-de-carriere', $this->importer->getTaxonomySlugsList( $terms ));
    }

    public function testGetTaxonomiesIds()
    {
        $slugs = 'production,certification-bio';
        $ids = $this->importer->getTaxonomiesIds($slugs, 'thematique');

        $this->assertInternalType('array', $ids);
        $this->assertNotNull($ids[0]);
    }

	/**
	 *
	 */
	public function testGetPostParameters()
    {
		$params = $this->importer->getPostParameters(
			$this->importer->getActions()[0]->action
		);

		$this->assertInternalType('array', $params);

		$this->assertArrayHasKey('post_title', $params);
		$this->assertArrayHasKey('post_author', $params);
        $this->assertEquals(1, $params['post_author']);

		$this->assertArrayHasKey('post_content', $params);
		$this->assertStringStartsWith('<h3>Une opportunit&eacute;', $params['post_content']);

		$this->assertArrayHasKey('post_status', $params);

        $this->assertArrayHasKey('post_date', $params);
        $this->assertNotNull($params['post_date']);
        $this->assertArrayHasKey('post_modified', $params);
        $this->assertNotNull($params['post_modified']);

        // Vérification des meta_input (données annexes)
		$this->assertArrayHasKey('meta_input', $params);

		$this->assertArrayHasKey('fip-id', $params['meta_input']);
		$this->assertArrayHasKey('action-phare', $params['meta_input']);
		$this->assertInternalType('int', $params['meta_input']['action-phare']);
		$this->assertArrayHasKey('lieu-et-horaire', $params['meta_input']);

		$this->assertArrayHasKey('inscription-debut', $params['meta_input']);
		$this->assertArrayHasKey('inscription-echeance', $params['meta_input']);

		$this->assertArrayHasKey('date-action', $params['meta_input']);

		$this->assertArrayHasKey('lien-document', $params['meta_input']);
		$this->assertArrayHasKey('lien-inscription', $params['meta_input']);

        $this->assertArrayHasKey('organisme', $params['meta_input']);


        // Vérification des taxonomies
        $this->assertArrayHasKey('tax_input', $params);
        $this->assertArrayHasKey('thematique', $params['tax_input']);
        $this->assertArrayHasKey('service', $params['tax_input']);

        $this->assertNotNull($params['tax_input']['thematique'][0]);
	}

	public function testImportPost()
    {
        $action = $this->importer->getActions()[0]->action;
        $post_id = $this->importer->insertPost(
            $action
        );

        // Vérification qu'on a bien inséré l'action
        $this->assertNotEquals(0, $post_id);

        // Tentative de réimport (doit conserver le même post_id)
        $second_post_id = $this->importer->insertPost(
            $action
        );

        $this->assertEquals($post_id, $second_post_id);


        // Vérification des thématiques et services insérés avec le post
        $this->assertEquals(sort( $this->importer->getTaxInput($action)['thematique']) , sort(
            array_map(
                function($term){
                    return $term->term_id;
                }, wp_get_post_terms($post_id, 'thematique'))
            )
        );

        $this->assertEquals(sort( $this->importer->getTaxInput($action)['service']) , sort(
                array_map(
                    function($term){
                        return $term->term_id;
                    }, wp_get_post_terms($post_id, 'service'))
            )
        );
    }
}