<?php
/**
 * Created by PhpStorm.
 * User: loicg
 * Date: 28/09/2016
 * Time: 12:01
 */

namespace Fip;


class TaxonomyImporter
{
    protected $taxonomy;
    protected $xml_url;

    protected $terms;

    public function __construct($taxonomy="category")
    {
        $this->taxonomy = $taxonomy;
        $this->xml_url = "";
    }

    public function loadTerms()
    {
        if(!file_exists(__DIR__ . '/../assets/listes_fip.json'))
            throw new \Exception('File not found :'. __DIR__ . '/../assets/listes_fip.json');

        $items = json_decode( file_get_contents(__DIR__ . '/../assets/listes_fip.json'));

        if(!property_exists($items, $this->taxonomy))
            throw new \Exception('Taxonomy "'.$this->taxonomy.'" does not exist in :'.__DIR__ . '/../assets/listes_fip.json');

        $this->terms = $items->{$this->taxonomy};
    }

    public function getTerms()
    {
        return $this->terms;
    }


    /**
     * Supprime les terms d'une taxinomie
     */
    public function flushTerms()
    {
        $terms = $this->getDbTerms();

        foreach($terms as $term){
            wp_delete_term($term->term_id, $this->taxonomy);
        }
    }

    /**
     * Lance l'import des termes
     */
    public function import()
    {
        $this->loadTerms();
        $this->importTerms($this->terms);
    }

    /**
     *
     * @param array $terms Les termes à insérer
     * @param int $parent Identifiant du terme parent (si besoin)
     */
    protected function importTerms($terms, $parent=0)
    {
        foreach($terms as $term){
            $created_term = wp_insert_term(
                $term->libelle,
                $this->taxonomy,
                [
                    'slug' => sanitize_title($term->libelle),
                    'description' => '',
                    'parent' => $parent,
                ]
            );

            if(is_array( $term->children) )
                $this->importTerms($term->children, $created_term['term_id']);
        }
    }

    /**
     * Récupère les termes de la taxinomie
     * @return array|int|\WP_Error
     */
    public function getDbTerms()
    {
        return get_terms([
           'taxonomy' => $this->taxonomy,
            'hide_empty' => false,
        ]);
    }

}